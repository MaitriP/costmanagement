USE [CostManagement]
GO
/****** Object:  StoredProcedure [dbo].[GetCostSummary]    Script Date: 30-05-2018 17:35:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[GetCostSummary] --2,2018,'00000000-0000-0000-0000-000000000000'
(
	@Month nvarchar(30),
	@Year nvarchar(30),
	@LocationID uniqueidentifier
)
AS
BEGIN
	
	Declare @MonthEndCheck as int
	select @MonthEndCheck = count(*) from MonthEnd Where CurrentMonth = @Month  and CurrentYear = @Year and LocationID = @LocationID

	
	Select * from CostSummery Where CurrentMonth = @Month 
								and CurrentYear = @Year 
								and LocationID = case @LocationID When '00000000-0000-0000-0000-000000000000' then LocationID Else @LocationID End
	
	Select @MonthEndCheck

	Select * from CostSummery Where CurrentMonth = Case @Month When 1 then 12 Else @Month - 1 End
									and CurrentYear = Case @Month When 1 then @Year - 1 Else @Year End
									and LocationID = case @LocationID When '00000000-0000-0000-0000-000000000000' then LocationID Else @LocationID End
END
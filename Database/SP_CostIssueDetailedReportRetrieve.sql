USE [CostManagement]
GO
/****** Object:  StoredProcedure [dbo].[CostIssueDetailedReportRetrieve]    Script Date: 30-05-2018 17:28:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select * from TransactionMaster
--[dbo].[CostIssueDetailedReportRetrieve] 'a87810da-cc65-4b07-937f-c4ef82da6485', 'df76576e-9ef3-4c57-bd96-b75d20ee7673',1,2018
ALTER PROCEDURE [dbo].[CostIssueDetailedReportRetrieve] --'a87810da-cc65-4b07-937f-c4ef82da6485', '06454447-7391-4f3d-a46f-13341d6d52a7',1,5,2018
(
	@CompanyID uniqueidentifier,
	@LocationID uniqueidentifier,
	@Month int,
	@Year int
)
AS
BEGIN

	--Table1	
	Select T.TransactionDate,  T.Sale,  T.Issue, isnull(ROUND(T.Issue*100/nullif(T.Sale,0),0),0) as IssueSalePercent, D.DepartmentName, D.ID as DepartmentID
	from 
	TransactionMaster T	inner join DepartmentMaster D
	on T.DepartmentID = D.ID
	Where LocationID = CASE @LocationID WHEN '00000000-0000-0000-0000-000000000000' THEN LocationID ELSE @LocationID END	
	and Month(TransactionDate) = @Month
	and Year(TransactionDate) = @Year	
	order by TransactionDate 

	--Table2
	Select TransactionDate, sum(Sale) As Sale, sum(Issue) As Issue, isnull(sum(ROUND(Issue*100/nullif(Sale,0),0)),0) as IssueSalePercent
	from 
	TransactionMaster	
	Where LocationID = CASE @LocationID WHEN '00000000-0000-0000-0000-000000000000' THEN LocationID ELSE @LocationID END
	and Month(TransactionDate) = @Month
	and Year(TransactionDate) = @Year
	Group By Transactiondate
	order by TransactionDate
	
	--Table3
	Select D.ID as DepartmentID, sum(T.Sale) As Sale, sum(T.Issue) As Issue, isnull(ROUND(sum(T.Issue)*100/nullif(sum(T.Sale),0),0),0) as IssueSalePercent
	from 
	TransactionMaster T	inner join DepartmentMaster D
	on T.DepartmentID = D.ID
	Where LocationID = CASE @LocationID WHEN '00000000-0000-0000-0000-000000000000' THEN LocationID ELSE @LocationID END
	and Month(TransactionDate) = @Month
	and Year(TransactionDate) = @Year
	Group By D.ID 
END


USE [CostManagement]
GO
/****** Object:  StoredProcedure [dbo].[DataByDate_Retrive]    Script Date: 30-05-2018 17:34:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[DataByDate_Retrive] -- '05/01/2018','df76576e-9ef3-4c57-bd96-b75d20ee7673',1
(
	@TransactionDate date,
	@LocationID uniqueidentifier,
	@DepartmentID int
)
AS
BEGIN
	select * from TransactionMaster where LocationID = @LocationID and DepartmentID = @DepartmentID and TransactionDate = @TransactionDate
END
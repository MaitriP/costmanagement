USE [CostManagement]
GO
/****** Object:  StoredProcedure [dbo].[InsertUpdateCostSummery]    Script Date: 30-05-2018 17:35:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from [Transaction]
ALTER PROCEDURE [dbo].[InsertUpdateCostSummery] --'00000000-0000-0000-0000-000000000000','4','2018','47454','43909','9687','19320','0','485e2694-291a-4f72-b105-92ffe810843f','485e2694-291a-4f72-b105-92ffe810843f'
(
	@ID uniqueidentifier,
	@Month nvarchar(50),
	@Year nvarchar(50),
	--@TotalIssue money,
	--@TotlaSale money,
	@OpenBal money,
	@CloseBal money,
	--@Damage money,
	@TransferOut money,
	@TransferIn money,
	@Less money,
	@FoodCost money,
	@CreatedBy uniqueidentifier,
	@ModifiedBy uniqueidentifier,
	@CompanyID uniqueidentifier,
	@LocationID uniqueidentifier,
	@OtherSale money,
	@OtherExp money
)
AS
BEGIN
	
	Declare @TotalSales as Money
	Declare @TotalIssue as Money
	Declare @ThisMonthDays as int
	Declare @LastMonthDays as int
	Declare @LastMonthTotalSale as Money
	Declare @ThisMonthAvgSale as money
	Declare @LastMonthAvgSale as money

	select @TotalIssue = Sum(Issue),  
		 @TotalSales = Sum(Sale)
		from TransactionMaster Where Month(TransactionDate) = @Month 
								and Year(TransactionDate) = @Year 
								and LocationID = @LocationID

	select @LastMonthTotalSale = Sum(Sale)
		from TransactionMaster Where Month(TransactionDate) = Case @Month When 1 then 12 Else @Month - 1 End
								and Year(TransactionDate) = Case @Month When 1 then @Year - 1 Else @Year End
								and LocationID = @LocationID

	select @ThisMonthDays = count(*) from TransactionMaster Where Month(TransactionDate) = @Month 
															 and Year(TransactionDate) = @Year
															 and LocationID = @LocationID
	select @LastMonthDays = count(*) from TransactionMaster Where Month(TransactionDate) = Case @Month When 1 then 12 Else @Month - 1 End
															 and Year(TransactionDate) = Case @Month When 1 then @Year - 1 Else @Year End
															 and LocationID = @LocationID

	Select @ThisMonthAvgSale = case when @TotalSales = NULL then  0 else @TotalSales / @ThisMonthDays end
	Select @LastMonthAvgSale = case when @LastMonthTotalSale = Null then 0 else @LastMonthTotalSale / @LastMonthDays end
	
	IF (@ID = '00000000-0000-0000-0000-000000000000')
	BEGIN
		Insert into CostSummery
		Values
		(
			NEWID(),
			@Month,
			@Year,
			@TotalIssue,
			@TotalSales,
			@OpenBal,
			@CloseBal,
			--@Damage,
			@Less,
			@FoodCost,
			@CreatedBy,
			@ModifiedBy,			
			GETDATE(),
			GETDATE(),
			@ThisMonthAvgSale,
			@LastMonthAvgSale,
			@TransferOut,
			@TransferIn,
			@CompanyID,
			@LocationID,
			@OtherSale,
			@OtherExp
		)
	END	
	Else
	BEGIN
		Update CostSummery
		set
			OpeningBalance = @OpenBal,
			ClosingBalance = @CloseBal,
			--Damage = @Damage,
			Less = @Less,
			ModifiedTS = GETDATE(),
			ModifiedBy = @ModifiedBy,
			TransferOut = @TransferOut,
			TransferIn = @TransferIn,
			CompanyID = @CompanyID,
			LocationID = @LocationID,
			ThisMonthAvgSale = @ThisMonthAvgSale,
			LastMonthAvgSale = @LastMonthAvgSale,
			OtherSale = @OtherSale,
			OtherExpense = @OtherExp
		Where ID = @ID
	END
END


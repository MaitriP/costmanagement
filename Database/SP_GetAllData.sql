USE [CostManagement]
GO
/****** Object:  StoredProcedure [dbo].[GetAllData]    Script Date: 30-05-2018 17:35:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[GetAllData]
	-- Add the parameters for the stored procedure here
	@TableName varchar(50)
AS
BEGIN

IF @TableName = 'CompanyMaster'
Begin
	Select * FROM CompanyMaster order by CompanyNo
End

IF @TableName = 'LocationMaster'
Begin
	Select L.ID,L.LocationName FROM LocationMaster L inner join CompanyMaster C
	on L.CompanyID = C.ID
	order by L.LocationName  
End

IF @TableName = 'DepartmentMaster'
Begin
	Select * FROM DepartmentMaster
End

End
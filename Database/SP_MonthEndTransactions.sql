USE [CostManagement]
GO
/****** Object:  StoredProcedure [dbo].[MonthEndTransactions]    Script Date: 30-05-2018 17:36:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[MonthEndTransactions]
(
	@Month nvarchar(50),
	@Year nvarchar(50),
	@CreatedBy uniqueidentifier,
	@CompanyID uniqueidentifier,
	@LocationID uniqueidentifier
)
AS
BEGIN

	Declare @TotalSales as Money
	Declare @TotalIssue as Money
	Declare @ThisMonthDays as int
	Declare @LastMonthDays as int
	Declare @LastMonthTotalSale as Money
	Declare @ThisMonthAvgSale as money
	Declare @LastMonthAvgSale as money

	select @TotalIssue = Sum(Issue),  
		 @TotalSales = Sum(Sale)
		from TransactionMaster Where Month(TransactionDate) = @Month 
								and Year(TransactionDate) = @Year 
								and LocationID = @LocationID

	select @LastMonthTotalSale = Sum(Sale)
		from TransactionMaster Where Month(TransactionDate) = Month(DATEADD(month, -1, TransactionDate)) 
								and Year(TransactionDate) = case Month(TransactionDate) when 1 then Year(DATEADD(year, -1, TransactionDate)) else @Year end 
								and LocationID = @LocationID

	select @ThisMonthDays = count(*) from TransactionMaster Where Month(TransactionDate) = @Month 
															 and Year(TransactionDate) = @Year
															 and LocationID = @LocationID
	select @LastMonthDays = count(*) from TransactionMaster  Where Month(TransactionDate) = Month(DATEADD(month, -1, TransactionDate))
															 and Year(TransactionDate) = case Month(TransactionDate) when 1 then Year(DATEADD(year, -1, TransactionDate)) else @Year end 
															 and LocationID = @LocationID

	Select @ThisMonthAvgSale = case when @TotalSales = NULL then  0 else @TotalSales / @ThisMonthDays end
	Select @LastMonthAvgSale = case when @LastMonthTotalSale = Null then 0 else @LastMonthTotalSale / @LastMonthDays end

	Declare @OB as money
	Declare @CL as money
	Declare @TO as money
	Declare @TI as money
	Declare @Less as money
	Declare @OS as money
	Declare @OE as money

	Select @OB = OpeningBalance, 
			@CL = ClosingBalance, 
			@TO= TransferOut, 
			@TI = TransferIn, 
			@Less = Less ,
			@OS = OtherSale,
			@OE = OtherExpense
			from CostSummery
	Where CurrentMonth = @Month and CurrentYear= @Year and LocationID = @LocationID

	Declare @TotalValue as money
	set @TotalValue = @TotalIssue + @OB - @CL - @TO + @TI - @Less 

	Declare @FoodCost as money
	set @FoodCost = @TotalValue * 100 / @TotalSales

	Insert into MonthEnd
	values
	(
		NEWID(),
		@Month,
		@Year,
		@TotalIssue,
		@TotalSales,
		@OB,
		@CL,
		@Less,
		@FoodCost,
		@TotalValue,
		@CreatedBy,
		@CreatedBy,
		GETDATE(),
		GETDATE(),
		@ThisMonthAvgSale,
		@LastMonthAvgSale,
		@TO,
		@TI,
		@CompanyID,
		@LocationID,
		@OS,
		@OE
	)
END
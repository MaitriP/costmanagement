USE [CostManagement]
GO
/****** Object:  StoredProcedure [dbo].[CheckMonthEndDetail]    Script Date: 30-05-2018 17:27:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[CheckMonthEndDetail]
(
	@Month nvarchar(10),
	@Year nvarchar(10),
	@LocationID uniqueidentifier
)
AS
BEGIN
	Select * from MonthEnd Where CurrentMonth = @Month and CurrentYear = @Year and LocationID = @LocationID
END
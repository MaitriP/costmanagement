USE [CostManagement]
GO
/****** Object:  StoredProcedure [dbo].[InsertUpdateTransaction]    Script Date: 30-05-2018 17:35:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from [Transaction]
ALTER PROCEDURE [dbo].[InsertUpdateTransaction]
(
	@ID uniqueidentifier,
	@UserID uniqueidentifier,
	@LocationID uniqueidentifier,
	@DepartmentID int,
	@TransactionDate date,
	@Issue money,
	@Sale money,
	@CreatedBy uniqueidentifier,
	@ModifiedBy uniqueidentifier 
)
AS
BEGIN
	
	IF (@ID = '00000000-0000-0000-0000-000000000000')
	BEGIN
		Insert into TransactionMaster
		Values
		(
			NEWID(),
			@UserID,
			@LocationID,
			@DepartmentID,
			@TransactionDate,
			@Issue,
			@Sale,
			GETDATE(),
			GETDATE(),
			@CreatedBy,
			@ModifiedBy
		)
	END
	ELSE
	BEGIN
		Update TransactionMaster
		set
			Issue = @Issue,
			Sale = @Sale,
			--TransactionDate = @TransactionDate,
			ModifiedTS = GETDATE()
		WHERE ID = @ID
	END

	--Update costSummery

	Declare @TotalSales as Money
	Declare @TotalIssue as Money
	Declare @ThisMonthDays as int
	Declare @LastMonthDays as int
	Declare @LastMonthTotalSale as Money
	Declare @ThisMonthAvgSale as money
	Declare @LastMonthAvgSale as money

	select @TotalIssue = Sum(Issue),  
		 @TotalSales = Sum(Sale)
		from TransactionMaster Where Month(TransactionDate) = Month(@TransactionDate)
								and Year(TransactionDate) = Year(@TransactionDate) 
								and LocationID = @LocationID

	select @LastMonthTotalSale = Sum(Sale)
		from TransactionMaster Where Month(TransactionDate)  = Case Month(@TransactionDate) When 1 then 12 Else Month(@TransactionDate) - 1 End
								and Year(TransactionDate) = Case Month(@TransactionDate) When 1 then Year(@TransactionDate) - 1 Else Year(@TransactionDate) End
								and LocationID = @LocationID

	select @ThisMonthDays = count(*) from TransactionMaster Where Month(TransactionDate) = Month(@TransactionDate)
																and Year(TransactionDate) = Year(@TransactionDate)
															 and LocationID = @LocationID
	select @LastMonthDays = count(*) from TransactionMaster Where Month(TransactionDate) = Month(@TransactionDate) - 1
															and Year(TransactionDate) = Case Month(@TransactionDate) When 1 then Year(@TransactionDate) - 1 Else Year(@TransactionDate) End
															 and LocationID = @LocationID

	Select @ThisMonthAvgSale = case when @TotalSales = NULL then  0 else @TotalSales / @ThisMonthDays end
	Select @LastMonthAvgSale = case when @LastMonthTotalSale = Null then 0 else @LastMonthTotalSale / @LastMonthDays end

	Update CostSummery
	set TotalIssue = @TotalIssue,
	TotalSale = @TotalSales,
	ModifiedBy = @ModifiedBy,
	ModifiedTS = GETDATE(),
	ThisMonthAvgSale = @ThisMonthAvgSale,
	LastMonthAvgSale = @LastMonthAvgSale
	Where CurrentMonth = Month(@TransactionDate) and
	CurrentYear = YEAR(@TransactionDate)
END
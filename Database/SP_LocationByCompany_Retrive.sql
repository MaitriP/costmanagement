USE [CostManagement]
GO
/****** Object:  StoredProcedure [dbo].[LocationByCompany_Retrive]    Script Date: 30-05-2018 17:36:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[LocationByCompany_Retrive]-- [LocationByCompany_Retrive] '8C0F1E5C-73F7-4468-8C98-22C8E06A3B65'	
	@CompanyId uniqueidentifier
AS
BEGIN

IF @CompanyId = '00000000-0000-0000-0000-000000000000'
Begin
	Select LocationMaster.Id,LocationName,CompanyID
	FROM LocationMaster 
	inner join CompanyMaster
	on LocationMaster.CompanyId = CompanyMaster.ID
	order by LocationMaster.LocationName 
End
Else
Begin
	Select LocationMaster.Id,LocationName,CompanyID
	FROM LocationMaster 
	inner join CompanyMaster
	on LocationMaster.CompanyId = CompanyMaster.ID
	Where LocationMaster.CompanyId = @CompanyId
	order by LocationMaster.LocationName 
End
END
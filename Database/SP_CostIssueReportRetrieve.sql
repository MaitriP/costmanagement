USE [CostManagement]
GO
/****** Object:  StoredProcedure [dbo].[CostIssueReportRetrieve]    Script Date: 30-05-2018 17:29:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from TransactionMaster
ALTER PROCEDURE [dbo].[CostIssueReportRetrieve] --'a87810da-cc65-4b07-937f-c4ef82da6485', '06454447-7391-4f3d-a46f-13341d6d52a7',1,1,2018
(
	@CompanyID uniqueidentifier,
	@LocationID uniqueidentifier,
	@DepartmentID int,
	@Month int,
	@Year int
)
AS
BEGIN

	DECLARE @Temp TABLE 
	(
		TransactionDate date,
		Sale money,
		Issue money,
		IssueSalePercent money
	)
	
	--Table1
	Insert @Temp
	Select TransactionDate, sum(Sale) As Sale, sum(Issue) As Issue, isnull(sum(ROUND(Issue*100/nullif(Sale,0),0)),0) as IssueSalePercent
	from 
	TransactionMaster	
	Where LocationID = CASE @LocationID WHEN '00000000-0000-0000-0000-000000000000' THEN LocationID ELSE @LocationID END
	and DepartmentID = CASE @DepartmentID WHEN 0 THEN DepartmentID ELSE @DepartmentID END 
	and Month(TransactionDate) = @Month
	and Year(TransactionDate) = @Year
	Group By Transactiondate
	order by TransactionDate 

	Select * from @Temp

	--Table2
	Select a.TransactionDate, (select sum(b.Sale) From @Temp b where b.TransactionDate <= a.TransactionDate) as CummSales,
	(select sum(b.Issue) From @Temp b where b.TransactionDate <= a.TransactionDate) as CummIssue,
	(select isnull(ROUND(sum(b.Issue)*100/sum(nullif(b.Sale,0)),0),0)From @Temp b where b.TransactionDate <= a.TransactionDate) as CummPercent
	from @Temp a
	order by a.TransactionDate

	

	--Table2
	--select 
 --   a.TransactionDate,		
 --   sum(b.Sale) as CummSales,
	--sum(b.Issue) as CummIssue,
	--ROUND(sum(b.Issue)*100/sum(b.Sale),0) as CummPercent
	--from TransactionMaster a 
	--join TransactionMaster b on a.TransactionDate >= b.TransactionDate	
	--Where a.LocationID = CASE @LocationID WHEN '00000000-0000-0000-0000-000000000000' THEN a.LocationID ELSE @LocationID END
	--and b.LocationID = CASE @LocationID WHEN '00000000-0000-0000-0000-000000000000' THEN b.LocationID ELSE @LocationID END
	--and a.DepartmentID = CASE @DepartmentID WHEN 0 THEN a.DepartmentID ELSE @DepartmentID END
	--and b.DepartmentID = CASE @DepartmentID WHEN 0 THEN b.DepartmentID ELSE @DepartmentID END
	--and Month(a.TransactionDate) = @Month
	--and Year(a.TransactionDate) = @Year
	--and Month(b.TransactionDate) = @Month
	--and Year(b.TransactionDate) = @Year
	--group by a.TransactionDate
	--order by a.TransactionDate

END


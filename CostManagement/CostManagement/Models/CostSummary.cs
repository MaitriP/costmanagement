﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CostManagement.Models
{
    public class CostSummary
    {
        public decimal Zero = 0;

        public Guid ID { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public decimal TotlaSale { get; set; }
        public decimal TotalIssue { get; set; }
        public decimal OpeningBalance { get; set; }
        public decimal ClosingBalance { get; set; }
        public decimal Damage { get; set; }
        public decimal Less { get; set; }
        public decimal FoodCost { get; set; }
        public decimal Total { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid ModifiedBy { get; set; }
        public decimal ThisMonthAvgSale { get; set; }
        public decimal LastMonthAvgSale { get; set; }
        public decimal ThisMonthFoodCost { get;set;}
        public decimal LastMonthFoodCost { get; set; }

        public string CompanyName { get; set; }
        public string LocationName { get; set; }

        public decimal TransferOut { get; set; }
        public decimal TransferIn { get; set; }

        public Guid CompanyID { get; set; }
        public Guid LocationID { get; set; }

        [DefaultValue(0)]
        public decimal OtherSale { get; set; }
        public decimal OtherExpense { get; set; }

        public decimal TotalSaleIncludingOtherSale { get; set; }
        public decimal ThisMonthFoodCostIncludingOtherSale { get; set; }
    }
}
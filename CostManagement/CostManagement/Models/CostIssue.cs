﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostManagement.Models
{
    public class CostIssue
    {
        public Guid TransactionID { get; set; }
        public Guid UserID { get; set; }
        public Guid LocationID { get; set; }
        public int DepartmentID { get; set; }
        public DateTime TransactionDate { get; set; }
        public decimal Issue { get; set; }
        public decimal Sale { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid ModifiedBy { get; set; }
        public Guid CompanyID { get; set; }

        public string DateString { get; set; }
    }

    public class CostIssueGridModel 
    {
        public DateTime TransactionDate { get; set; }
        public decimal Issue { get; set; }
        public decimal Sale { get; set; }
        public decimal IssueSalePercent { get; set; }
        public decimal CummIssue { get; set; }
        public decimal CummSales { get; set; }
        public decimal CummPercent { get; set; }

        public string DepartmentName { get; set; }
        public int DepartmentID { get; set; }
    }

    public class CostIssueDetailReportTotalsModel
    {
        public DateTime TransactionDate { get; set; }
        public decimal Issue { get; set; }
        public decimal Sale { get; set; }
        public decimal IssueSalePercent { get; set; }
        public int DepartmentID { get; set; }
    }
}
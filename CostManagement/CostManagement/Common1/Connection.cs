﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace CostManagement.Common1
{
    public class Connection
    {
        public string MasterConnection = string.Empty;
        public static SqlConnection conn = null;
        public SqlDataAdapter adp;

        public Connection()
        {
            MasterConnection = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        }

        public DataSet GetData(Hashtable hs, string SpName)
        {
            DataSet ds = new DataSet();
            try
            {
                conn = new SqlConnection(MasterConnection);
                conn.Open();
                using (conn)
                {
                    SqlCommand cmd = new SqlCommand(SpName, conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    foreach (string param in hs.Keys)
                    {
                        cmd.Parameters.AddWithValue(param, hs[param]);
                    }
                    adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);
                }
                conn.Close();
            }
            catch (Exception e)
            {
                WriteError("\n GetData: " + SpName + " : " + e.Message);
                //error
            }
            return ds;
        }

        public string InsertData(Hashtable hs, string spName)
        {
            try
            {
                conn = new SqlConnection(MasterConnection);
                conn.Open();
                using (conn)
                {
                    SqlCommand cmd = new SqlCommand(spName, conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    foreach (string param in hs.Keys)
                    {
                        cmd.Parameters.AddWithValue(param, hs[param]);
                    }
                    var result = cmd.ExecuteNonQuery();
                    return result.ToString();
                }
            }
            catch (Exception e) { WriteError("\n InsertData: " + spName + " : " + e.Message); return "0"; }
        }

        public DataTable GetAllData(string TableName)
        {
            DataTable dt = new DataTable();
            try
            {
                conn = new SqlConnection(MasterConnection);
                conn.Open();
                using (conn)
                {
                    SqlCommand cmd = new SqlCommand("GetAllData", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TableName", TableName);
                    adp = new SqlDataAdapter(cmd);
                    adp.Fill(dt);
                }
                conn.Close();
            }
            catch (Exception e)
            {
                WriteError("\n GetAllData: " + TableName + " : " + e.Message);
                //error
            }
            return dt;
        }

        public void WriteError(string message)
        {
            string lines = "Message: " + message + ".\n Current Date is: " + System.DateTime.Now.ToString() + ". \n";
            System.IO.Directory.SetCurrentDirectory(System.AppDomain.CurrentDomain.BaseDirectory);
            System.IO.StreamWriter file = System.IO.File.AppendText("LogFile.txt");// new System.IO.StreamWriter("D:\\Maitri\\Projects\\WindowService\\TestEmailWinService\\TestEmailWinService\\LogFile.txt");
            file.WriteLine(lines);
            file.Close();
        }
    }
}
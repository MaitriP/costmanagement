﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CostManagement.Common1
{
    public class Dictionary
    {
        public static Dictionary<string, string> Months = new Dictionary<string, string>()
        {
              {    "1",  "January"},
              {    "2",   "February"},
              {    "3",   "March"},
              {    "4",   "April"},
              {    "5",   "May"},
              {    "6",   "June"},
              {    "7",   "July"},
              {    "8",   "August"},
              {    "9",   "September"},
              {    "10",   "October"},
              {    "11",   "November"},
              {    "12",   "December"}
        };

        SelectList Month = new SelectList(new[]
    {
        new {Value = "1", Text="January"},
            new {Value = "2", Text="February"},
            new {Value = "3", Text="March"},
            new {Value = "4", Text="April"},
            new {Value = "5", Text="May"},
            new {Value = "6", Text="June"},
            new {Value = "7", Text="July"},
            new {Value = "8", Text="August"},
            new {Value = "9", Text="September"},
            new {Value = "10", Text="October"},
            new {Value = "11", Text="November"},
            new {Value = "12", Text="December"}
    },
    "Value", "Text", 1);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostManagement.Common1
{
    public class Message
    {
        public string LoginFailed =  "Login Failed, Please ensure about valid credentials.";
        public string SuccessMessage = "Data Inserted Successfully.";
        public string ErrorMessage = "Data is not inserted. Please try again.";

        public string AlreadyExistingData = "You have already entered data for the selection.";

        public string NoCostSummery = "Please enter cost summery data for selected month.";
    }
}
﻿using CostManagement.Common1;
using CostManagement.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CostManagement.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Login/
        Connection conn;
        Message mes;
        public ActionResult Login()
        {
            TempData["ErrorMessage"] = "";
            TempData["SuccessMessage"] = "";
            return View();
        }

        [HttpPost]
        public ActionResult ValidateLogin(User userViewModel)
        {
            conn = new Connection();
            mes = new Message();
            DataSet ds = new DataSet();
            Hashtable hs = new Hashtable();
            try
            {
                if (userViewModel.UserName != null && userViewModel.Password != null)
                {
                    hs.Add("@UserName",userViewModel.UserName);
                    hs.Add("@Password",userViewModel.Password);
                    ds = conn.GetData(hs, "ValidateLogin");
                    if (ds.Tables[0].Rows.Count == 1)
                    {
                        Session["UserID"] = ds.Tables[0].Rows[0]["ID"].ToString();
                        Session["UserName"] = ds.Tables[0].Rows[0]["UserName"].ToString();
                    }
                    else
                    {
                        TempData["ErrorMessage"] = mes.LoginFailed;
                        conn.WriteError(mes.LoginFailed);
                        return RedirectToAction("Login", "Login");
                    }
                }
                return RedirectToAction("Dashboard","Dashboard");
            }
            catch (Exception e)
            {
                conn.WriteError("ValidateLogin :" + e.Message);
                return RedirectToAction("Login","Login");
            }
        }

        public ActionResult Logout()
        {
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("LogIn");
        }

    }
}

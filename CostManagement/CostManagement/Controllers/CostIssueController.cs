﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Mvc;
using CostManagement.Models;
using CostManagement.Common1;
using System.Collections;
using System.Data;
using Newtonsoft.Json;
using PdfSharp;
using PdfSharp.Drawing;
using Pechkin;
using Pechkin.Synchronized;
using System.Net;
using Common.Logging.Factory;
using System.Web.UI;
using System.Drawing.Printing;
using System.Web.Script.Serialization;
using System.Globalization;

namespace CostManagement.Controllers
{
    public class CostIssueController : Controller
    {
        //
        // GET: /CostIssue/
        Connection conn;
        Message mes;
        public ActionResult CostIssue(bool FirstLoad = true)
        {
            if (FirstLoad)
                Session["DropdownSelection"] = new CostIssue { CompanyID = Guid.Empty, LocationID = Guid.Empty, DepartmentID = 0 };
            GetDefaultData();
            return View();
        }

        public void GetDefaultData()
        {
            conn = new Connection();
            Hashtable hs = new Hashtable();
            DataTable dt = new DataTable();
            try
            {
                hs = new Hashtable();
                dt = conn.GetAllData("CompanyMaster");
                var company = from c in dt.AsEnumerable()
                              select new SelectListItem
                              {
                                  Value = c.Field<Guid>("ID").ToString(),
                                  Text = c.Field<string>("CompanyName").ToString()
                              };
                ViewData["Company"] = (List<SelectListItem>)company.ToList();

                dt = conn.GetAllData("LocationMaster");
                var Location = from c in dt.AsEnumerable()
                               select new SelectListItem
                               {
                                   Value = c.Field<Guid>("ID").ToString(),
                                   Text = c.Field<string>("LocationName").ToString()
                               };
                ViewData["Location"] = (List<SelectListItem>)Location.ToList();

                dt = conn.GetAllData("DepartmentMaster");
                var Department = from c in dt.AsEnumerable()
                                 select new SelectListItem
                                 {
                                     Value = c.Field<int>("ID").ToString(),
                                     Text = c.Field<string>("DepartmentName").ToString()
                                 };
                ViewData["Department"] = (List<SelectListItem>)Department.ToList();

                Session["TotalPercent"] = "0.0";

            }
            catch (Exception e)
            {
                conn.WriteError("GetDefaultData :" + e.Message);
            }
        }

        [HttpPost]
        public ActionResult CostIssueAdd(CostIssue csObj, FormCollection frm)
        {
            conn = new Connection();
            mes = new Message();
            Hashtable hs = new Hashtable();
            try
            {
                if (csObj != null)
                {
                    if (csObj.TransactionID != Guid.Empty)
                        hs.Add("@ID", csObj.TransactionID);
                    else
                    {
                        #region "Check for existing"
                        JsonResult Result = GetDataByDate(frm["TransactionDate"].ToString(), csObj.LocationID.ToString(), csObj.DepartmentID.ToString());
                        //JsonResult Result = GetDataByDate(DateTime.ParseExact(csObj.TransactionDate.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", CultureInfo.InvariantCulture).ToString(), csObj.LocationID.ToString(), csObj.DepartmentID.ToString());
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        List<CostIssue> ExistResult = serializer.Deserialize<List<CostIssue>>(serializer.Serialize(Result.Data));
                        if (ExistResult.Count() == 0)
                        {
                            hs.Add("@ID", Guid.Empty);
                            TempData["AlreadyExistingData"] = "";
                        }
                        else
                        {
                            TempData["AlreadyExistingData"] = mes.AlreadyExistingData;
                            return RedirectToAction("CostIssue");
                        }
                        #endregion
                    }
                    hs.Add("@UserID", Session["UserID"]);
                    hs.Add("@LocationID", csObj.LocationID);
                    hs.Add("@DepartmentID", csObj.DepartmentID);
                    hs.Add("@TransactionDate", frm["TransactionDate"].ToString());
                    //hs.Add("@TransactionDate", Convert.ToDateTime(frm["TransactionDate"].ToString()));
                    hs.Add("@Issue", csObj.Issue);
                    hs.Add("@Sale", csObj.Sale);
                    hs.Add("@CreatedBy", Session["UserID"]);
                    hs.Add("@ModifiedBy", Session["UserID"]);
                    var session = new CostIssue { CompanyID = new Guid(frm["CompanyID"].ToString()), LocationID = csObj.LocationID, DepartmentID = csObj.DepartmentID, DateString = frm["TransactionDate"].ToString() };
                    Session["DropdownSelection"] = session;

                    var result = conn.InsertData(hs, "InsertUpdateTransaction");
                    if (result == "1" || result == "2")
                    {
                        TempData["SuccessMessage"] = mes.SuccessMessage;
                    }
                    else
                    {
                        TempData["ErrorMessage"] = mes.ErrorMessage;
                    }

                }

                return RedirectToAction("CostIssue", new { FirstLoad = false });
            }
            catch (Exception e)
            {
                conn.WriteError("CostIssueAdd :" + e.Message);
                return RedirectToAction("CostIssue");
            }
        }

        [HttpPost]
        public JsonResult GetLocationById(string CompanyId)
        {
            //Get Data for Location Filter
            try
            {
                DataSet ds;
                DataTable dt;
                conn = new Connection();
                Hashtable hs = new Hashtable();
                hs.Add("@CompanyId", CompanyId);

                ds = new DataSet();
                ds = conn.GetData(hs, "LocationByCompany_Retrive");
                var location = from c in ds.Tables[0].AsEnumerable()
                               select new SelectListItem
                               {
                                   Value = c.Field<Guid>("ID").ToString(),
                                   Text = c.Field<string>("LocationName").ToString()
                               };
                ViewData["Location"] = location.ToList();
                return Json(location.ToList());
            }
            catch (Exception e)
            {
                conn.WriteError("\n GetLocationById : " + e.Message);
                return Json("Error");
            }
        }

        [HttpPost]
        public JsonResult GetDataByDate(string TransactionDate, string LocationID, string DepartmentID)
        {
            try
            {
                DataSet ds;
                conn = new Connection();
                Hashtable hs = new Hashtable();
                hs.Add("@TransactionDate", TransactionDate.ToString());
                //hs.Add("@TransactionDate", DateTime.ParseExact(TransactionDate, "dd/MM/yyyy", CultureInfo.GetCultureInfo("en-gb")));
                hs.Add("@LocationID", LocationID);
                hs.Add("@DepartmentID", DepartmentID);

                ds = new DataSet();
                ds = conn.GetData(hs, "DataByDate_Retrive");
                var CostIssue = from c in ds.Tables[0].AsEnumerable()
                                select new CostIssue
                                {
                                    TransactionID = c.Field<Guid>("ID"),
                                    TransactionDate = Convert.ToDateTime(c.Field<DateTime>("TransactionDate").ToString()),
                                    Issue = Convert.ToDecimal(c.Field<decimal>("Issue").ToString()),
                                    Sale = Convert.ToDecimal(c.Field<decimal>("Sale").ToString())
                                };
                ViewData["CostIssue"] = CostIssue.ToList();
                return Json(CostIssue.ToList());
            }
            catch (Exception e)
            {
                conn.WriteError("\n GetDataByDate : " + e.Message);
                return Json("Error");
            }
        }

        public ActionResult CostIssueListReport1()
        {
            GetDefaultData();
            return View();
        }

        public string GetReport(string Month, string Year, string LocationID, string DepartmentID, string CompanyID)
        {
            DataSet ds;
            Hashtable hs;
            conn = new Connection();
            try
            {
                GetDefaultData();
                ds = new DataSet();
                hs = new Hashtable();
                hs.Add("@LocationID", LocationID != "null" ? LocationID : Guid.Empty.ToString());
                hs.Add("@DepartmentID", DepartmentID == "" ? "0" : DepartmentID);
                hs.Add("@Month", Month);
                hs.Add("@Year", Year);
                hs.Add("@CompanyID", CompanyID != "" ? CompanyID : Guid.Empty.ToString());
                var ReportResult = BindCostIssueReport1Data(hs);


                #region "Bind Footer Section"
                //ds = new DataSet();
                //hs = new Hashtable();
                //hs.Add("@Month", Month);
                //hs.Add("@Year", Year);
                //hs.Add("@LocationID", LocationID);
                //ds = conn.GetData(hs, "GetCostSummary");
                //CostSummary csObj = new CostSummary();
                //Session["MessageBottom"] = "";
                //if (ds.Tables[0].Rows.Count > 0)
                //{
                //    csObj.OpeningBalance = Convert.ToDecimal(ds.Tables[0].Rows[0]["OpeningBalance"].ToString());
                //    csObj.ClosingBalance = Convert.ToDecimal(ds.Tables[0].Rows[0]["ClosingBalance"].ToString());
                //    csObj.TotalIssue = Convert.ToDecimal(ds.Tables[0].Rows[0]["TotalIssue"].ToString());
                //    csObj.TotlaSale = Convert.ToDecimal(ds.Tables[0].Rows[0]["TotalSale"].ToString());
                //    csObj.TransferOut = Convert.ToDecimal(ds.Tables[0].Rows[0]["TransferOut"].ToString());
                //    csObj.TransferIn = Convert.ToDecimal(ds.Tables[0].Rows[0]["TransferIn"].ToString());
                //    csObj.Less = Convert.ToDecimal(ds.Tables[0].Rows[0]["Less"].ToString());
                //    csObj.FoodCost = Convert.ToDecimal(ds.Tables[0].Rows[0]["FoodCost"].ToString());
                //    string MessageBottom = "Sale : " + csObj.TotlaSale + "Issue : " + csObj.TotalIssue + "Average Sale : Opening Balance : " + csObj.OpeningBalance;
                //    Session["MessageBottom"] = csObj;
                //}

                #endregion

                string str = string.Empty;
                if (ReportResult != null)
                    str = JsonConvert.SerializeObject(ReportResult.ToList(), Formatting.None);
                return str;
            }
            catch (Exception e)
            {
                conn.WriteError("\n GetReport : " + e.Message);
                return null;
            }
        }

        public IEnumerable<CostIssueGridModel> BindCostIssueReport1Data(Hashtable hs)
        {
            conn = new Connection();
            DataSet ds = new DataSet();
            try
            {
                ds = conn.GetData(hs, "CostIssueReportRetrieve");
                IEnumerable<CostIssue> ReportResult = new List<CostIssue>();
                IEnumerable<CostIssueGridModel> joinTable = new List<CostIssueGridModel>();
                if (ds.Tables[1].Rows.Count > 0)
                {
                    DataTable dt1 = ds.Tables[0];
                    DataTable dt2 = ds.Tables[1];
                    joinTable = (from p in dt1.AsEnumerable()
                                 join t in dt2.AsEnumerable()
                                     on Convert.ToDateTime(p.Field<DateTime>("TransactionDate").ToString()) equals Convert.ToDateTime(t.Field<DateTime>("TransactionDate").ToString())
                                 //orderby Convert.ToDateTime(p.Field<DateTime>("TransactionDate").ToString())
                                 select new CostIssueGridModel
                                 {
                                     TransactionDate = Convert.ToDateTime(p.Field<DateTime>("TransactionDate").ToString()),
                                     Issue = Convert.ToDecimal(p.Field<decimal>("Issue").ToString()),
                                     Sale = Convert.ToDecimal(p.Field<decimal>("Sale").ToString()),
                                     IssueSalePercent = Convert.ToDecimal(p.Field<decimal>("IssueSalePercent").ToString()),
                                     CummIssue = Convert.ToDecimal(t.Field<decimal>("CummIssue").ToString()),
                                     CummSales = Convert.ToDecimal(t.Field<decimal>("CummSales").ToString()),
                                     CummPercent = Convert.ToDecimal(t.Field<decimal>("CummPercent").ToString())
                                 });

                    object TotalSale = dt1.Compute("Sum(Sale)", "");

                    Session["TotalPercent"] = dt2.Rows[dt2.Rows.Count - 1]["CummPercent"].ToString();
                    //Session["ThingsToPrint"] = ThingsToPrint;
                    return joinTable.OrderBy(x => x.TransactionDate);
                }
                return joinTable.ToList();
            }
            catch (Exception e)
            {
                conn.WriteError("BindCostIssueReport1Data :" + e.Message);
                return null;
            }
        }

        #region "Cost Summary"
        public ActionResult AddCostSummary()
        {
            GetDefaultData();
            return View();
        }

        [HttpPost]
        public ActionResult CostSummaryAdd(CostSummary csObj, FormCollection frm)
        {
            conn = new Connection();
            Hashtable hs = new Hashtable();
            try
            {
                if (csObj != null)
                {

                    if (csObj.ID != Guid.Empty)
                        hs.Add("@ID", csObj.ID);
                    else
                    {
                        #region "Check for existing"
                        JsonResult Result = GetCostSummeryData(csObj.Month.ToString(), csObj.Year.ToString(), csObj.LocationID.ToString());
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        List<CostIssue> ExistResult = serializer.Deserialize<List<CostIssue>>(serializer.Serialize(Result.Data));
                        if (ExistResult.Count() == 0)
                        {
                            hs.Add("@ID", Guid.Empty);
                            TempData["AlreadyExistingData"] = "";
                        }
                        else
                        {
                            TempData["AlreadyExistingData"] = mes.AlreadyExistingData;
                            return RedirectToAction("AddCostSummary");
                        }
                        #endregion

                    }
                    hs.Add("@Month", csObj.Month);
                    hs.Add("@Year", frm["Year"]);
                    hs.Add("@OpenBal", csObj.OpeningBalance);
                    hs.Add("@CloseBal", csObj.ClosingBalance);
                    //hs.Add("@Damage", csObj.Damage);
                    hs.Add("@TransferOut", csObj.TransferOut);
                    hs.Add("@TransferIn", csObj.TransferIn);
                    hs.Add("@Less", csObj.Less);
                    hs.Add("@FoodCost", 0);
                    hs.Add("@CreatedBy", Session["UserID"]);
                    hs.Add("@ModifiedBy", Session["UserID"]);
                    hs.Add("@CompanyID", frm["CompanyID"]);
                    hs.Add("@LocationID", frm["LocationID"]);
                    hs.Add("@OtherSale", csObj.OtherSale);
                    hs.Add("@OtherExp", csObj.OtherExpense);

                    var result = conn.InsertData(hs, "InsertUpdateCostSummery");
                }
            }
            catch (Exception e)
            {
                conn.WriteError("CostSummaryAdd :" + e.Message);
            }
            return RedirectToAction("Dashboard", "Dashboard");
        }

        //[HttpPost]
        //public ActionResult CostIssueSummaryReport(string Month, string Year, string LocationID, string DepartmentID, string CompanyID, string CompanyName, string LocationName)
        public ActionResult CostIssueSummaryReport(string Month, string Year, string LocationID, string DepartmentID, string CompanyID, string CompanyName, string LocationName)
        {
            DataSet ds;
            Hashtable hs;
            conn = new Connection();
            mes = new Message();

            #region "Get GridData"
            ds = new DataSet();
            hs = new Hashtable();
            hs.Add("@LocationID", LocationID != "null" ? LocationID : Guid.Empty.ToString());
            hs.Add("@DepartmentID", DepartmentID);
            hs.Add("@Month", Month);
            hs.Add("@Year", Year);
            hs.Add("@CompanyID", CompanyID != "" ? CompanyID : Guid.Empty.ToString());
            IEnumerable<CostIssueGridModel> ReportResult = BindCostIssueReport1Data(hs);

            #endregion

            #region "Bind Footer Section"
            ds = new DataSet();
            hs = new Hashtable();
            hs.Add("@Month", Month);
            hs.Add("@Year", Year);
            hs.Add("@LocationID", LocationID);
            ds = conn.GetData(hs, "GetCostSummary");
            CostSummary csObj = new CostSummary();
            Session["MessageBottom"] = "";
            if (ds.Tables[0].Rows.Count > 0)
            {
                csObj.Month = Dictionary.Months[ds.Tables[0].Rows[0]["CurrentMonth"].ToString()].ToString();
                csObj.Year = ds.Tables[0].Rows[0]["CurrentYear"].ToString();
                csObj.OpeningBalance = Convert.ToDecimal(ds.Tables[0].Rows[0]["OpeningBalance"].ToString());
                csObj.ClosingBalance = Convert.ToDecimal(ds.Tables[0].Rows[0]["ClosingBalance"].ToString());
                csObj.TotalIssue = Convert.ToDecimal(ds.Tables[0].Rows[0]["TotalIssue"].ToString());
                csObj.TotlaSale = Convert.ToDecimal(ds.Tables[0].Rows[0]["TotalSale"].ToString());
                csObj.OtherSale = Convert.ToDecimal(ds.Tables[0].Rows[0]["OtherSale"].ToString());
                csObj.TransferOut = Convert.ToDecimal(ds.Tables[0].Rows[0]["TransferOut"].ToString());
                csObj.TransferIn = Convert.ToDecimal(ds.Tables[0].Rows[0]["TransferIn"].ToString());
                csObj.Less = Convert.ToDecimal(ds.Tables[0].Rows[0]["Less"].ToString());
                csObj.FoodCost = Convert.ToDecimal(ds.Tables[0].Rows[0]["FoodCost"].ToString());
                csObj.OtherExpense = Convert.ToDecimal(ds.Tables[0].Rows[0]["OtherExpense"].ToString());
                csObj.Total = csObj.TotalIssue + csObj.OpeningBalance - csObj.ClosingBalance - csObj.TransferOut + csObj.TransferIn - csObj.Less - csObj.OtherExpense;
                csObj.ThisMonthAvgSale = Convert.ToDecimal(ds.Tables[0].Rows[0]["ThisMonthAvgSale"].ToString());
                csObj.LastMonthAvgSale = Convert.ToDecimal(ds.Tables[0].Rows[0]["LastMonthAvgSale"].ToString() == "" ? "0" : ds.Tables[0].Rows[0]["LastMonthAvgSale"].ToString());
                csObj.ThisMonthFoodCost = csObj.Total * 100 / csObj.TotlaSale;
                csObj.CompanyName = CompanyName;
                csObj.LocationName = LocationName;
                csObj.TotalSaleIncludingOtherSale = csObj.TotlaSale + csObj.OtherSale;
                csObj.ThisMonthFoodCostIncludingOtherSale = csObj.Total * 100 / csObj.TotalSaleIncludingOtherSale;

                if (ds.Tables[2].Rows.Count > 0)
                {
                    decimal LastMonthTotal = Convert.ToDecimal(ds.Tables[2].Rows[0]["TotalIssue"].ToString())
                                            + Convert.ToDecimal(ds.Tables[2].Rows[0]["OpeningBalance"].ToString())
                                            - Convert.ToDecimal(ds.Tables[2].Rows[0]["ClosingBalance"].ToString())
                                            - Convert.ToDecimal(ds.Tables[2].Rows[0]["TransferOut"].ToString())
                                            + Convert.ToDecimal(ds.Tables[2].Rows[0]["TransferIn"].ToString())
                                            - Convert.ToDecimal(ds.Tables[2].Rows[0]["Less"].ToString());
                    csObj.LastMonthFoodCost = LastMonthTotal * 100 / Convert.ToDecimal(ds.Tables[2].Rows[0]["TotalSale"].ToString());
                }
                else
                {
                    csObj.LastMonthFoodCost = 0;
                }

                Session["MessageBottom"] = csObj;
            }
            else
            {
                TempData["NoCostSummery"] = mes.NoCostSummery;
                return RedirectToAction("CostIssueListReport1");
            }

            #endregion

            if (ReportResult == null)
            {
                return null;
            }
            else
            {
                string html = RenderRazorViewToString("CostIssueSummaryReport", ReportResult.ToList());
                //string html = "<html>Hello World</html>";
                try
                {
                    //byte[] pdfBuf = new SynchronizedPechkin(new GlobalConfig()).Convert(html);
                    ObjectConfig oc = new ObjectConfig();
                    oc.Footer.SetCenterText("[page] of [topage]");
                    SynchronizedPechkin sc = new SynchronizedPechkin(new GlobalConfig().SetMargins(new Margins(40, 20, 60, 50))
                   .SetDocumentTitle("Sale/Issue").SetCopyCount(1).SetImageQuality(50)//.SetPageOffset(1)
                   .SetLosslessCompression(true).SetMaxImageDpi(120).SetOutlineGeneration(true).SetOutputDpi(1200).SetPaperOrientation(true)
                   .SetPaperSize(PaperKind.A4Rotated));

                    byte[] pdfBuf = sc.Convert(oc, html);

                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-Disposition", string.Format("attachment;filename=IssueSaleReport.pdf; size={0}", pdfBuf.Length));
                    Response.BinaryWrite(pdfBuf);

                    Response.Flush();
                    Response.End();
                    return View(ReportResult.ToList());
                }
                catch (Exception e)
                {
                    conn.WriteError("CostIssueSummaryReport : " + e.Message);
                    return null;
                }
            }
        }

        public string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext,
                                                                         viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View,
                                             ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        [HttpPost]
        public JsonResult GetCostSummeryData(string Month, string Year, string LocationID)
        {
            DataSet ds;
            conn = new Connection();
            Hashtable hs = new Hashtable();
            try
            {
                hs.Add("@Month", Month);
                hs.Add("@Year", Year);
                hs.Add("@LocationID", LocationID);
                ds = new DataSet();
                ds = conn.GetData(hs, "GetCostSummary");
                if (ds.Tables[1].Rows[0][0].ToString() == "0")
                {
                    var CostSummary = from c in ds.Tables[0].AsEnumerable()
                                      select new CostSummary
                                      {
                                          ID = c.Field<Guid>("ID"),
                                          OpeningBalance = Convert.ToDecimal(c.Field<decimal>("OpeningBalance").ToString()),
                                          ClosingBalance = Convert.ToDecimal(c.Field<decimal>("ClosingBalance").ToString()),
                                          TransferOut = Convert.ToDecimal(c.Field<decimal>("TransferOut").ToString()),
                                          TransferIn = Convert.ToDecimal(c.Field<decimal>("TransferIn").ToString()),
                                          Less = Convert.ToDecimal(c.Field<decimal>("Less").ToString()),
                                          OtherSale = Convert.ToDecimal(c.Field<decimal>("OtherSale").ToString()),
                                          OtherExpense = Convert.ToDecimal(c.Field<decimal>("OtherExpense").ToString()),
                                      };
                    ViewData["CostSummary"] = CostSummary.ToList();
                    return Json(CostSummary.ToList());
                }
                else
                    return Json("MonthEnd");
            }
            catch (Exception e)
            {
                conn.WriteError("\n GetDataByDepartment : " + e.Message);
                return Json("Error");
            }
        }

        #endregion

        #region "MonthEnd"

        public ActionResult MonthEnd()
        {
            GetDefaultData();
            return View();
        }

        [HttpPost]
        public ActionResult MonthEndTransaction(FormCollection frm)
        {
            conn = new Connection();
            Hashtable hs = new Hashtable();
            try
            {
                hs.Add("@Month", frm["Month"]);
                hs.Add("@Year", frm["Year"]);
                hs.Add("@CreatedBy", Session["UserID"]);
                hs.Add("@CompanyID", frm["CompanyID"]);
                hs.Add("@LocationID", frm["LocationID"]);
                var result = conn.InsertData(hs, "MonthEndTransactions");

            }
            catch (Exception e)
            {
                conn.WriteError("MonthEndTransaction :" + e.Message);
            }
            return RedirectToAction("Dashboard", "Dashboard");
        }

        [HttpPost]
        public JsonResult GetMonthEndedDetails(string Month, string Year, string LocationID)
        {
            Hashtable hs = new Hashtable();
            conn = new Connection();
            DataSet ds = new DataSet();
            int rowCount = 0;
            try
            {
                hs.Add("@Month", Month);
                hs.Add("@Year", Year);
                hs.Add("@LocationID", LocationID);
                ds = conn.GetData(hs, "CheckMonthEndDetail");
                rowCount = ds.Tables[0].Rows.Count;
            }
            catch (Exception ex)
            {
                conn.WriteError("GetMonthEndedDetails :" + ex.Message);
            }
            return Json(rowCount);
        }

        #endregion

        #region "Cost Issue Detailed Report"

        public ActionResult CostIssueDetailedReport()
        {
            GetDefaultData();
            return View();
        }

        public string GetDetailedReport(string Month, string Year, string LocationID, string CompanyID)
        {
            DataSet ds;
            Hashtable hs;
            conn = new Connection();
            try
            {
                GetDefaultData();
                ds = new DataSet();
                hs = new Hashtable();
                hs.Add("@LocationID", LocationID != "null" ? LocationID : Guid.Empty.ToString());
                hs.Add("@Month", Month);
                hs.Add("@Year", Year);
                hs.Add("@CompanyID", CompanyID != "" ? CompanyID : Guid.Empty.ToString());
                var ReportResult = BindCostIssueDetailedReportData(hs,false);

                string str = string.Empty;
                if (ReportResult != null)
                    str = JsonConvert.SerializeObject(ReportResult.ToList(), Formatting.None);
                return str;
            }
            catch (Exception e)
            {
                conn.WriteError("\n GetDetailedReport : " + e.Message);
                return null;
            }
        }

        public IEnumerable<CostIssueGridModel> BindCostIssueDetailedReportData(Hashtable hs, bool pdf=false)
        {
            conn = new Connection();
            DataSet ds = new DataSet();
            try
            {
                ds = conn.GetData(hs, "CostIssueDetailedReportRetrieve");
                IEnumerable<CostIssueGridModel> ReportResult = new List<CostIssueGridModel>();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DataTable dt1 = ds.Tables[0];

                    ReportResult = (from p in dt1.AsEnumerable()
                                    select new CostIssueGridModel
                                    {
                                        TransactionDate = Convert.ToDateTime(p.Field<DateTime>("TransactionDate").ToString()),
                                        Issue = Convert.ToDecimal(p.Field<decimal>("Issue").ToString()),
                                        Sale = Convert.ToDecimal(p.Field<decimal>("Sale").ToString()),
                                        IssueSalePercent = Convert.ToDecimal(p.Field<decimal>("IssueSalePercent").ToString()),
                                        DepartmentName = p.Field<string>("DepartmentName").ToString(),
                                        DepartmentID = p.Field<int>("DepartmentID")
                                    });

                    
                }
                #region "Total section for PDF Creation"
                if (pdf)
                {
                    DataTable dt = new DataTable();
                    dt = ds.Tables[1];
                    IEnumerable<CostIssueDetailReportTotalsModel> DatewiseTotal = new List<CostIssueDetailReportTotalsModel>();
                    DatewiseTotal = (from p in dt.AsEnumerable()
                                     select new CostIssueDetailReportTotalsModel
                                     {
                                         TransactionDate = Convert.ToDateTime(p.Field<DateTime>("TransactionDate").ToString()),
                                         Issue = Convert.ToDecimal(p.Field<decimal>("Issue").ToString()),
                                         Sale = Convert.ToDecimal(p.Field<decimal>("Sale").ToString()),
                                         IssueSalePercent = Convert.ToDecimal(p.Field<decimal>("IssueSalePercent").ToString())
                                     });
                    Session["DatewiseTotal"] = DatewiseTotal.ToList();

                    dt = new DataTable();
                    dt = ds.Tables[2];
                    IEnumerable<CostIssueDetailReportTotalsModel> DeptwiseTotal = new List<CostIssueDetailReportTotalsModel>();
                    DeptwiseTotal = (from p in dt.AsEnumerable()
                                     select new CostIssueDetailReportTotalsModel
                                     {
                                         Issue = Convert.ToDecimal(p.Field<decimal>("Issue").ToString()),
                                         Sale = Convert.ToDecimal(p.Field<decimal>("Sale").ToString()),
                                         IssueSalePercent = Convert.ToDecimal(p.Field<decimal>("IssueSalePercent").ToString()),
                                         DepartmentID = p.Field<int>("DepartmentID")
                                     });
                    Session["DeptwiseTotal"] = DeptwiseTotal.ToList();
                }
                #endregion
                //return ReportResult.ToList();
                return ReportResult.OrderBy(x => x.TransactionDate);
            }
            catch (Exception e)
            {
                conn.WriteError("BindCostIssueDetailedReportData :" + e.Message);
                return null;
            }
        }

        public ActionResult CostIssueSummaryDetailedReport(string Month, string Year, string LocationID, string CompanyID, string CompanyName, string LocationName)
        {
            DataSet ds;
            Hashtable hs;
            DataTable dt;
            conn = new Connection();
            mes = new Message();

            #region "Get GridData"
            ds = new DataSet();
            hs = new Hashtable();
            hs.Add("@LocationID", LocationID != "null" ? LocationID : Guid.Empty.ToString());
            hs.Add("@Month", Month);
            hs.Add("@Year", Year);
            hs.Add("@CompanyID", CompanyID != "" ? CompanyID : Guid.Empty.ToString());
            IEnumerable<CostIssueGridModel> ReportResult = BindCostIssueDetailedReportData(hs,true);
            #endregion

            CostSummary csObj = new CostSummary();
            Session["DetailesData"] = "";
            csObj.Month = Dictionary.Months[Month.ToString()].ToString();
            csObj.Year = Year;
            csObj.CompanyName = CompanyName;
            csObj.LocationName = LocationName;
            Session["DetailesData"] = csObj;

            dt = conn.GetAllData("DepartmentMaster");
            var Department = from c in dt.AsEnumerable()
                             select new Department
                             {
                                 ID = c.Field<int>("ID"),
                                 DepartmentName = c.Field<string>("DepartmentName").ToString()
                             };
            Session["Department"] = Department.ToList();

            if (ReportResult == null)
            {
                return null;
            }
            else
            {
                string html = RenderRazorViewToString("CostIssueSummaryDetailedReport", ReportResult.ToList());
                try
                {
                    ObjectConfig oc = new ObjectConfig();
                    oc.Footer.SetRightText("[page] of [topage]");
                    oc.Footer.SetContentSpacing(2);
                    SynchronizedPechkin sc = new SynchronizedPechkin(new GlobalConfig().SetMargins(new Margins(40, 20, 60, 50))
                   .SetDocumentTitle("Sale/Issue").SetCopyCount(1).SetImageQuality(50)//.SetPageOffset(1)
                   .SetLosslessCompression(true).SetMaxImageDpi(120).SetOutlineGeneration(true).SetOutputDpi(1200).SetPaperOrientation(true)
                   .SetPaperSize(PaperKind.A4));

                    byte[] pdfBuf = sc.Convert(oc, html);

                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-Disposition", string.Format("attachment;filename=IssueSaleDetailedReport.pdf; size={0}", pdfBuf.Length));
                    Response.BinaryWrite(pdfBuf);

                    Response.Flush();
                    Response.End();
                    return View(ReportResult.ToList());
                }
                catch (Exception e)
                {
                    conn.WriteError("CostIssueSummaryReport : " + e.Message);
                    return null;
                }
            }

        #endregion
        }
    }
}
